#[macro_use]
extern crate array_macro;
extern crate csv;
extern crate rand;
extern crate rayon;
extern crate time;

pub mod initial_solution;
pub mod model;
pub mod parser;
pub mod solver;

pub use solver::Solver;
