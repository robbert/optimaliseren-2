extern crate grote_opdracht;

use grote_opdracht::parser;
use grote_opdracht::Solver;
use std::env::{args, current_exe, set_current_dir};
use std::fs::File;
use std::sync::Arc;

const DATA_DIR: &str = "data";
const DISTANCES_FILE: &str = "distances.txt";
const ORDERS_FILE: &str = "orders.txt";

fn main() {
    let max_iterations = args()
        .nth(1)
        .and_then(|iterations| match iterations.parse() {
            Ok(iterations) => Some(iterations),
            Err(_) => {
                eprintln!("\nUsage:\nsolver <iterations>\nDefaulting to 1000000 iterations");
                None
            }
        })
        .unwrap_or(2_000_000);

    set_working_directory();
    let travel_time = Arc::new(
        parser::parse_distances(File::open(DISTANCES_FILE).expect("Could not find distances.txt"))
            .expect("Could not parse distances.txt"),
    );
    let orders = Arc::new(
        parser::parse_orders(File::open(ORDERS_FILE).expect("Could not find orders.txt"))
            .expect("Could not parse orders.txt"),
    );

    println!("Finished loading!");
    println!("Distance matrix: {} entries", travel_time.len());
    println!("Orders: {} entries", orders.len());
    println!("\nCommencing the great search for glory!\n");

    let state = Solver::new(
        &Arc::clone(&orders),
        &Arc::clone(&travel_time),
        max_iterations,
    ).solve();

    println!("\nOutput:\n{}", state);
    println!("Resulting score: {}", state.score());
    println!("\nUnscheduled orders:");
    for order_id in &state.possible_orders {
        let order = &orders[order_id];
        println!(
            "{} :: frequency: {}, duration: {}, total duration: {}, volume {}",
            order_id,
            order.frequency,
            order.duration,
            order.frequency as i32 * order.duration,
            order.volume
        );
    }

    state.debug_validate();
}

/// Finds the data files relative to the binary and sets working directory. Returns `false` if the
/// data files could not be found.
fn set_working_directory() -> bool {
    let mut path = current_exe().expect("Could not get the binary's path");
    path.pop();

    loop {
        let data_dir = path.join(DATA_DIR);
        if data_dir.join(DISTANCES_FILE).exists() && data_dir.join(ORDERS_FILE).exists() {
            set_current_dir(data_dir).expect("Unable to set the working directory");
            return true;
        }

        // If we still can't find the directory at the file system root then it probably doesn't
        // exist
        if !path.pop() {
            return false;
        }
    }
}
