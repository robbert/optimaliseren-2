use csv;
use std::error::Error;
use std::io::Read;

use model::{Order, OrderList, Time, TravelTime, TIME_PER_MINUTE, TIME_PER_SECOND};

pub fn parse_distances<R: Read>(file: R) -> Result<TravelTime, Box<Error>> {
    let mut csv = csv::ReaderBuilder::new().delimiter(b';').from_reader(file);
    let mut result = TravelTime::new();
    for row in csv.records() {
        let row = row?;
        let from_id = row[0].parse()?;
        let to_id = row[1].parse()?;
        let time: Time = row[3].parse()?;

        result.insert((from_id, to_id), time * TIME_PER_SECOND);
    }

    Ok(result)
}

pub fn parse_orders<R: Read>(file: R) -> Result<OrderList, Box<Error>> {
    let mut csv = csv::ReaderBuilder::new().delimiter(b';').from_reader(file);
    let mut result = OrderList::new();
    for row in csv.records() {
        let row = row?;
        let id = row[0].parse()?;
        let frequency = row[2][..1].parse()?;
        let container_count: i32 = row[3].parse()?;
        let container_volume: i32 = row[4].parse()?;
        let duration: f32 = row[5].parse()?;
        let location_id = row[6].parse()?;
        let volume = (container_count * container_volume) / 5;

        result.insert(
            id,
            Order {
                location_id: location_id,
                frequency: frequency,
                duration: (duration * TIME_PER_MINUTE as f32).round() as Time,
                volume: volume,
            },
        );
    }

    Ok(result)
}
