#!/usr/bin/env bash

set -e

cd "$(dirname "$0")"
rm -rf submit
mkdir submit

cd verslag-2
latexmk
cp verslag.pdf ../submit
cd ..

cp solution.txt submit

mkdir submit/code
git archive -o submit/code/repo.tar origin/master
cd submit/code
tar -xf repo.tar
rm repo.tar
cd ..

tar -caf ../submit.tar.gz *
cd ..
rm -rf submit
