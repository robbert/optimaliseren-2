\documentclass[12pt,a4paper,dutch]{scrartcl}

\usepackage[dutch]{babel}
\usepackage{lmodern,textcomp}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage[pdftex]{hyperref}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{float}
\usepackage{subcaption}
\usepackage[newfloat]{minted}

\setminted{frame=leftline,framesep=1em,linenos,numbersep=1em,style=pastie}

% \setenumerate{label=\textit{\alph*})}
\addtokomafont{labelinglabel}{\sffamily\bfseries}

\makeatletter
\AtBeginDocument{
	\hypersetup{
		pdftitle = {\@title},
		pdfauthor = Robbert van der Helm \& Melvin Groot \& Steppe Röttgering,
		pdfsubject = {Optimaliseren \& Complexiteit}
	}
}
\makeatother

\renewcommand{\and}{\vspace{1cm}}

\title{Optimaliseren \& Complexiteit}
\subtitle{Grote Opdracht}
\author{Robbert van der Helm\\5620724 \and Melvin Groot\\5606543 \and Steppe
  Röttgering\\5548594}

\begin{document}

\maketitle
\tableofcontents
\clearpage

\section{Introductie}
In deze opdracht proberen we de optimale oplossing van een NP-lastig
optimaliseringsprobleem te benaderen behulp van lokaal zoeken. We hebben hier te
maken met een \emph{Multi Vehicle Routing} problem. Hierin moeten twee
vrachtwagens van een afvalverwerkingsbedrijf over de duur van een week zo
efficiënt mogelijk vuilnis ophalen bij hun klanten, rekening houdend met
werktijden, capaciteiten en de wensen van de klant. De exacte formulering van
het probleem staan in sectie~\ref{section:probleembeschrijving}. Aan de hand
hiervan maken we een beginoplossing die we met een metaheuristische zoekmethoden
kunnen verbeteren. Onze exacte aanpak staat verder in het verslag beschreven.

\section{Gebruik van de solver}
De solver is geïmplementeerd in \href{https://www.rust-lang.org/en-US/}{Rust}.
Het programma is als volgt te compilen en te draaien:

\begin{minted}{bash}
  cargo run --release  # Draai met de standaard 200.000 iteraties
  cargo run --release -- <iteraties>  # Draai het meegegeven aantal iteraties
\end{minted}

\noindent
De API docs van de solver en zijn dependencies staan gehost op
\url{https://grote-opdracht.robbertvanderhelm.nl/}. Deze zijn ook te genereren:

\begin{minted}{bash}
  cargo docs --open
\end{minted}

\label{section:probleembeschrijving}
\section{Probleembeschrijving}
Het probleem is als volgt gedefinieerd:

\begin{itemize}[itemsep=-2pt]
  \item Twee vrachtwagens die zonder pauzes kunnen rijden.
  \item Maandag t/m vrijdag; 06:00-18:00. Vóór 18.00 uur auto’s leeg en terug
    bij het afvalverwerkingsbedrijf.
  \item Routes beginnen en eindigen bij het afvalverwerkingsbedrijf.
  \item Frequentie van ophalen variëert per order varieert tussen de een en vijf
    keer per week.
  \item Afstandenmatrix is een Cartesisch product tussen alle adressen.
  \item De maximale capaciteit van een vrachtwagen is 20.000 liter.
    \begin{itemize}[noitemsep]
    \item Containers worden vijf keer gecomprimeerd. Hierdoor is het totale
      volume van een order het aantal containers keer het volume per container
      gedeeld door vijf.
    \end{itemize}
  \item Vrachtwagen legen kost 30 minuten.
    \begin{itemize}[noitemsep]
    \item Legen mag altijd en zo vaak nodig is.
    \item Beide auto’s mogen tegelijk geleegd worden.
    \end{itemize}
  \item Orders moeten geheel of niet uitgevoerd worden.
  \item Uiteindelijke score is een som van:
    \begin{itemize}[noitemsep]
    \item Totale tijd bezig met legen containers
    \item Totale reistijd
    \item Totale tijd bezig met afval dumpen
    \item Drie maal de tijd die nodig zou zijn voor het legen van niet-geplande
      orders
    \end{itemize}
\end{itemize}

\noindent
We moeten ons ook houden aan de volgende toegestane patronen:

\bigskip
\begin{tabular}{l|l|p{18em}}
  \textbf{\textsf{Indicatie}} & \textbf{\textsf{Frequentie}} & \textbf{\textsf{Toegestande patronen}} \\
  1PWK & Eens per week & ma, di, wo, do, vr \\
  2PWK & Twee keer per week & ma\_do, di\_vr \\
  3PWK & Drie keer per week & ma\_wo\_vr \\
  4PWK & Vier keer per week & ma\_di\_wo\_do, ma\_di\_wo\_vr, ma\_di\_do\_vr, ma\_wo\_do\_vr, di\_wo\_do\_vr \\
  5PWK & Vijf keer per week & ma\_di\_wo\_do\_vr \\
\end{tabular}

\section{Datastructuren}
We modelleren het probleem als een array van arrays die lijsten van events bevat
in het formaat `\mintinline{rust}{state.events[voertuig][dag][event_ID]}'.
Hierin is een
\texttt{\href{https://grote-opdracht.robbertvanderhelm.nl/grote_opdracht/model/enum.Event.html}{Event}}
een order met bijbehorend ordernummer of een dump. Al deze events worden samen
met huidige score, de nog niet ingeplande orders, de dagen waarop bestaande
orders ingepland zijn en alle order- en afstandsgegevens opgeslagen in een
\texttt{\href{https://grote-opdracht.robbertvanderhelm.nl/grote_opdracht/model/struct.State.html}{State}}\emph{-object}.
Deze state bevat verder een aantal operators om de state zelf aan te passen. Ter
efficiëntie is elke operator beschikbaar in drie vormen:

\begin{description}
\item[\texttt{<operator>()}] Hiermee wordt de operator in zijn volledigheid
  uitgevoerd. Dit geeft een \mintinline{rust}{Result<(), StateError>} terug. Als
  een operator om een reden niet uitgevoerd kan worden, dan geeft deze
  \texttt{\href{https://grote-opdracht.robbertvanderhelm.nl/grote_opdracht/model/enum.StateError.html}{StateError}}
  aan waarom dit niet kon.
\item[\texttt{try\_<operator>()}] Deze functies worden gebruikt tijdens het
  aanroepen van de normale operator en tijdens het zoeken naar vervolgstaat. Het
  resultaat bestaat weer uit een \texttt{StateError} als het niet mogelijk is om
  de operator uit te voeren en een tuple met onder andere het scoreverschil
  wanneer dit wel kan. Omdat deze functies de state zelf niet aan kunnen passen
  kunnen ze efficiënt in parallel gebruikt worden om een vervolgstaat te vinden.
\item[\texttt{<operator>\_precalculated()}] Deze functies passen een operator
  toe met het resultaat van de bijbehorende \mintinline{rust}{try_<operator>()}
  functie. Dit is alleen ter optimalisatie voor het zoeken bedoeld. Daarom
  voeren deze functies geen bound checks uit. Het kan daarom onveilig zijn om
  deze functies met de verkeerde gegevens aan te roepen.
\end{description}

\label{section:operators}
\subsection{Operators}
\begin{description}
\item[
  \href{https://grote-opdracht.robbertvanderhelm.nl/grote_opdracht/model/struct.State.html\#method.insert_order}{Insert
    order}] Plaats een nog niet ingeplande order in een event lijst op de
  meegegeven voertuig, dag en event indices. Als hierdoor een vrachtwagen over
  zijn capaciteit zou gaan dan wordt er meteen een nieuwe dump ingepland. Voor
  het inplannen van een event aan het eind van een dag bestaat nog een extra
  \href{https://grote-opdracht.robbertvanderhelm.nl/grote_opdracht/model/struct.State.html\#method.insert_order_last}{hulpoperator}.

\item[
  \href{https://grote-opdracht.robbertvanderhelm.nl/grote_opdracht/model/struct.State.html\#method.remove_event}{Remove
    event}] Verwijder een event. Dit event kan zowel een dump als een order
  zijn. Als er een order verwijderd wordt, dan wordt deze meteen uit alle dagen
  waarop hij ingepland stond verwijderd. Hierdoor wordt het onmogelijk om een
  order deels in te plannen.
\item[
  \href{https://grote-opdracht.robbertvanderhelm.nl/grote_opdracht/model/struct.State.html\#method.swap_events}{Swap
    events}] Wissel twee events op dezelfde dag om. Hierbij is het mogelijk om
  tussen twee voertuigen te wisselen.
\item[
  \href{https://grote-opdracht.robbertvanderhelm.nl/grote_opdracht/model/struct.State.html\#method.yolo}{Yolo}]
  Her-arrangeer alle events binnen een dag. Deze operator is vooral nuttig als
  een dag niet helemaal vol zit.
\end{description}

\section{Beginoplossing}
We bouwen de begin oplossing op een greedy, stochastische manier. Het doel
hierbij is om zo veel mogelijk orders van tevoren al in de state op te slaan,
met als achterliggende gedachte dat het hierdoor makkelijker wordt om de routes
te verbeteren door de gebeurtenissen op een dag onderling te wisselen. Voor elk
voertuig en voor elke dag proberen we een route op te bouwen die telkens de
meest nabije order inplant. Als een order op meerdere dagen ingepland moet
worden, dan kiezen we stochastisch een combinatie van voertuigen en dagen die
uiteindelijk een goede score zou opleveren. Dit hele proces wordt acht keer in
parallel uitgevoerd, waarna de state met de laagste startscore wordt gekozen.
Dit geeft een startoplossing met een score van tussen de 6000 en de 6100. De
functie die bij deze beginoplossing hoort is
\href{https://grote-opdracht.robbertvanderhelm.nl/grote_opdracht/initial_solution/fn.create_greedy_stochastic_state.html}{create\_greedy\_stochastic\_state()}.

\section{Zoekmethode}
We proberen een goede oplossing te vinden door de gevonden beginoplossing te
verbeteren met behulp van een parallelle \emph{simulated annealing} aanpak.
Binnen een iteratie genereren we herhaaldelijk willekeurig acties. Als de
gekozen actie geldig is en het resultaat ervan voldoet aan
$e^{\frac{-\text{scoreverschil}}{\text{temperatuur}}} > p$, waar $p$ een
willekeurig gekozen getal tussen de 0 en de 1 is, dan wordt deze actie
geaccepteerd en daadwerkelijk uitgevoerd op de state. Dit zoeken kan
geparallelliseerd worden omdat het kiezen van een actie het State object zelf
niet aanpast. De temperatuur wordt bepaald door middel van \emph{quadratic
  additive} koelschema. Hierbij wordt de temperatuur voor iteratie $k$ van de
$n$ berekend door te interpoleren tussen een start- en een eindtemperatuur met
de formule $T_k = T_n + (T_0 - T_n) (\frac{n - k}{n})^2$. Het verloop van de
score en de temperatuur tijdens het draaien van de solver is te zien in
figuur~\ref{fig:score}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.8\linewidth]{./graph.pdf}
  \caption{Verloop van score en temperatuur}
  \label{fig:score}
\end{figure}

De scores worden gaandeweg bijgehouden en aangepast. We berekenen voor elke
actie alleen het verschil in score ten opzichte van de vorige state. Tijdens
elke iteratie kijken we eerst wat een verandering zou opleveren, voordat we die
daadwerkelijk uitvoeren. De score wordt één maal als geheel berekend voor de
begin oplossing, daarna wordt het scoreverschil per operatie incrementeel
berekend.

\subsection{Acties}
Tijdens het zoeken kunnen een aantal acties worden uitgevoerd. Deze komen
overeen met de operators zoals gedefinieerd in sectie~\ref{section:operators}.
Om te bepalen welke actie geprobeerd moet worden genereren we een willekeurige
\href{https://grote-opdracht.robbertvanderhelm.nl/grote_opdracht/solver/enum.PossibleAction.html}{\texttt{PossibleAction}}:

\begin{description}
\item[Insert \emph{(35\% kans)}] Voeg een nog niet ingeplande order ergens in
  het rooster toe. Als hierdoor de lading van de vrachtwagen boven zijn
  capaciteit uit zou komen, dan wordt er een nieuwe dump vlag voor deze order
  ingepland.
\item[Remove \emph{(1\% kans)}] Verwijder een willekeurig event. Als dit een
  order is dan worden deze order op alle ingeplande dagen verwijderd zodat hij
  niet deels ingepland kan zijn.
\item[Remove low frequency \emph{(5\% kans)}] Verwijder een willekeurig event.
  Als dit een order is, dan wordt deze alleen verwijderd als zijn frequency een
  of twee is.
\item[Swap \emph{(57\% kans)}] Wissel twee events binnen een dag om. Deze events
  mogen in andere voertuigen zitten.
\item[Yolo \emph{(2\% kans)}] Herarrangeer alle events van een voertuig die
  binnen een dag gebeuren.
\end{description}

\subsection{RNG}
Voor het kiezen van onze willekeurige getallen hebben we een afweging tussen
snelheid en cryptografische veiligheid gemaakt. We gebruiken een
\href{https://grote-opdracht.robbertvanderhelm.nl/rand/fn.weak_rng.html}{Xor
  Shift} pseudo random number generator die telkens voor elke overwogen actie
opnieuw geseed wordt met een cryptografisch veilige RNG. Deze RNG wordt op zijn
beurt weer na elke 32 bytes opnieuw geseed met de random number generator van
het systeem. Op deze manier is het genereren van willekeurige getallen snel,
maar toch willekeurig genoeg voor onze doeleinden.

\subsection{Parallelisme}
Voor het paralleliseren gebruiken we de
\href{https://github.com/rayon-rs/rayon}{\emph{Rayon}} library. Deze library
gebruikt achter de schermen work stealing thread pools en geeft ons toegang tot
parallele iterators, sorteerfuncties, devide-and-conquer en meer.

\section{Parameters}
\begin{tabular}{l|l}
  \textsf{\textbf{Starttemperatuur}} & 242.0 \\
  \textsf{\textbf{Eindtemperatuur}} & 6.9 \\
  \textsf{\textbf{Aantal iteraties (gekozen acties)}} & 200.000 - 1.000.000
\end{tabular}

\medskip
\noindent
Het verhogen van het aantal iteraties levert kleine verbeteringen in de
uiteindelijk gevonden oplossing op. Vanaf ongeveer 200.000 iteraties wordt dit
scoreverschil geleidelijk aan steeds minder. Hierbij is een iteratie een
geaccepteerde operatie.

\section{Prestaties}
Het aantal iteraties per seconden hangt af van het gekozen maximaal aantal
iteraties. DIt komt doordat de solver bij een hoger aantal maximaal iteraties
meer iteraties doorbrengt met lagere temperaturen waarbij minder snel acties
worden gekozen. Voor een maximaal aantal iteraties van 200.000 ligt het aantal
gekozen acties per seconden rond de 3800. Bij 1.000.000 iteraties zijn dit 3300
uitgevoerde acties per seconde. Het aantal overwogen acties ligt altijd rond de
300.000. Tijdens het draaien gebruikt de solver 46 MB aan RAM. Deze resultaten
zijn behaald met een Intel i7 4790k, rustc 1.24.0-nightly en de Linux kernel
versie 4.14.7.

\section{Ideeën en verbeteringen}
\begin{itemize}
\item We hebben nagedacht over het tussentijds optimaliseren van de
  beginoplossing voordat we verder gingen met het daadwerkelijke
  simulated-annealing proces, door simulated annealing op de individuele dagen
  en voertuigen toe te passen. Dit gaf een goede beginoplossing, maar het maakte
  het lastig om deze te verbeteren zonder hem weer helemaal om te gooien. Daarom
  hebben we uiteindelijk gekozen voor een snellere oplossing. Een paar van de
  andere beginoplossingen zijn te vinden in de
  \href{https://grote-opdracht.robbertvanderhelm.nl/grote_opdracht/initial_solution/index.html}{\emph{initial\_solution}}
  module.
\item Om een goede beginoplossing te maken bepalen we in parallel meerdere
  beginoplossingen en kiezen daar dan de beste van.
\item We vergelijken meerdere acties in parallel om bij lagere temperaturen
  sneller een actie te kunnen kiezen.
\item We hebben meerdere koelschema's vergeleken, waaronder een adaptieve aanpak
  die de temperatuur verhoogde als er te lang geen verbetering in score
  plaatsvond.
\end{itemize}

\section{Gebreken en missende features}
\begin{itemize}
\item Meer operators. Orders met een frequentie van 1 kunnen vrij met andere
  soortgelijke orders gewisseld worden. Dit zou het makkelijker maken om tussen
  verschillende dagen te optimaliseren.
\end{itemize}

\section{Beste gevonden oplossing}
De beste oplossing die we hebben gevonden levert een score van \textbf{5714.31}
op. Deze is te vinden in \texttt{solution.txt}.

\end{document}