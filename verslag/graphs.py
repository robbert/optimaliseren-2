#!/usr/bin/env python3

import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv('graphs.csv', index_col=0)

plt.style.use('seaborn-darkgrid')
ax = df.plot(secondary_y=('Temperatuur'), figsize=(8, 6))
plt.tight_layout()
plt.savefig('graph.pdf', bbox_inches='tight')
# plt.show()
